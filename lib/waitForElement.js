function waitForElement(selector) {
    return new Promise((resolve, reject) => {
        let el = document.querySelector(selector);
        if (el) {
            resolve(el);
        }
        new MutationObserver((mutationRecords, observer) => {
            Array.from(document.querySelectorAll(selector)).forEach((element) => {
                resolve(element);
                observer.disconnect();
            });
        }).observe(document.documentElement, {
            childList: true,
            subtree: true,
        });
    });
}

function waitForElementVisible(selector) {
    return new Promise((resolve, reject) => {
        let el = document.querySelector(selector);
        if (el && isVisible(el)) {
            resolve(el);
        }
        new MutationObserver((mutationRecords, observer) => {
            Array.from(document.querySelectorAll(selector)).forEach((element) => {
                if (isVisible(element)) {
                    resolve(element);
                    observer.disconnect();
                }
            });
        }).observe(document.documentElement, {
            childList: true,
            subtree: true,
            attributes: true,
        });
    });
}

function waitForElementEnable(selector) {
    return new Promise((resolve, reject) => {
        let el = document.querySelector(selector);
        if (el && isEnable(el)) {
            resolve(el);
        }
        new MutationObserver((mutationRecords, observer) => {
            Array.from(document.querySelectorAll(selector)).forEach((element) => {
                if (isEnable(element)) {
                    resolve(element);
                    observer.disconnect();
                }
            });
        }).observe(document.documentElement, {
            childList: true,
            subtree: true,
            attributes: true,
        });
    });
}

function waitForElementClassDisappear(selector, classAttribute) {
    return new Promise((resolve, reject) => {
        let el = document.querySelector(selector);
        if (el && !el.classList.contains(classAttribute)) {
            resolve(el);
        }
        new MutationObserver((mutationRecords, observer) => {
            Array.from(document.querySelectorAll(selector)).forEach((element) => {
                if (!element.classList.contains(classAttribute)) {
                    resolve(element);
                    observer.disconnect();
                }
            });
        }).observe(document.documentElement, {
            childList: true,
            subtree: true,
            attributes: true,
        });
    });
}

function waitForElementContentChange(selector, changedContent) {
    return new Promise((resolve, reject) => {
        let el = document.querySelector(selector);
        if (el && el.innerText == changedContent) {
            resolve(el);
        }
        new MutationObserver((mutationRecords, observer) => {
            Array.from(document.querySelectorAll(selector)).forEach((element) => {
                if (el.innerText == changedContent) {
                    resolve(element);
                    observer.disconnect();
                }
            });
        }).observe(document.documentElement, {
            childList: true,
            subtree: true,
            characterData: true,
        });
    });
}

function isVisible(elem) {
    if (!(elem instanceof Element)) throw Error('elem is not an element.');
    const style = getComputedStyle(elem);
    if (style.display === 'none') return false;
    if (style.visibility !== 'visible') return false;
    if (style.opacity < 0.1) return false;
    if (elem.offsetWidth + elem.offsetHeight + elem.getBoundingClientRect().height + elem.getBoundingClientRect().width === 0) {
        return false;
    }
    const elemCenter = {
        x: elem.getBoundingClientRect().left + elem.offsetWidth / 2,
        y: elem.getBoundingClientRect().top + elem.offsetHeight / 2,
    };
    if (elemCenter.x < 0) return false;
    if (elemCenter.x > (document.documentElement.clientWidth || window.innerWidth)) return false;
    if (elemCenter.y < 0) return false;
    if (elemCenter.y > (document.documentElement.clientHeight || window.innerHeight)) return false;
    let pointContainer = document.elementFromPoint(elemCenter.x, elemCenter.y);
    do {
        if (pointContainer === elem) return true;
    } while (pointContainer = pointContainer.parentNode);
    return false;
}

function isEnable(elem) {
    return !elem.hasAttribute('disabled');
}
