var ready = function ready(callback) {
    var cb = function cb() {
        return setTimeout(callback, 0);
    };

    if (document.readyState !== 'loading') {
        cb();
    } else {
        document.addEventListener('DOMContentLoaded', cb);
    }

    return this;
};