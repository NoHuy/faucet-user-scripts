// ==UserScript==
// @name         PTC Share Visual Captcha auto resolve
// @namespace    Violentmonkey Scripts
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        http://www.ptcshare.com/member/paid-ads-view-*
// @grant        none
// @require      http://localhost:3000/lib/resemble.js
// @require      http://localhost:3000/lib/dictionary.js
// @require      http://localhost:3000/lib/waitForElement.js
// ==/UserScript==
(function() {
    'use strict';

    window.onload = waitForElementVisible('#captcha').then((captcha) => {
        // alert delay code execution, no good.
        alert = (message) => { console.log(message); };

        var captchaExplaination = captcha.querySelector('.visualCaptcha-explanation strong').innerHTML;
        var imgs = Array.from(captcha.querySelectorAll('img[id^=\'visualCaptcha-img\']'));
        var sourceimg = dictionary[captchaExplaination];

        imgs.forEach(img => {
            resemble(sourceimg)
                .compareTo(img.src)
                .setReturnEarlyThreshold(8)
                .onComplete(function(data) {
                    if (data.rawMisMatchPercentage < 3) {
                        let rect = img.getBoundingClientRect();
                        click(rect.x, rect.y);
                        document.getElementById('captcha_button').click();

                        waitForElementVisible('#nextAdBtn').then(next => {
                            next.click();
                        });
                    }
                });
        });
    });

    function click(x, y)
    {
        var ev = new MouseEvent('click', {
            'view': window,
            'bubbles': true,
            'cancelable': true,
            'screenX': x,
            'screenY': y
        });

        var el = document.elementFromPoint(x, y);

        el.dispatchEvent(ev);
    }
})();
