// ==UserScript==
// @name        Moon faucet auto open modal
// @namespace   Violentmonkey Scripts
// @match       https://moondoge.co.in/faucet
// @match       http://moonbit.co.in/faucet
// @match       https://moonliteco.in/faucet
// @match       http://moonbitcoin.cash/faucet
// @match       http://moondash.co.in/faucet
// @match       http://bitfun.co/dice
// @require     http://localhost:3000/lib/ready.js
// @require     http://localhost:3000/lib/waitForElement.js
// @grant       none
// @version     1.0
// @author      -
// @description 8/22/2020, 8:50:52 PM
// ==/UserScript==
(function() {
    ready(() => {
        waitForElementEnable('button[data-target="#ClaimModal"]').then((element) => {
            element.click();
        });
    });
})();