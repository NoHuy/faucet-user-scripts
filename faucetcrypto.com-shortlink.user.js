// ==UserScript==
// @name        faucetcrypto.com - short link
// @namespace   Violentmonkey Scripts
// @require     http://localhost:3000/lib/ready.js
// @require     http://localhost:3000/lib/waitForElement.js
// @match       https://faucetcrypto.com/claim*
// @match       https://claim4.fun/BCH/*
// @match       https://claim4.fun/BTC/*
// @match       https://claim4.fun/ETH/*
// @match       https://faucet.gold/BTC/*
// @match       https://faucet.gold/BCH/*
// @match       https://faucet.gold/ETH/*
// @grant       none
// @version     1.0
// @author      -
// @description 10/24/2020, 6:29:17 AM
// ==/UserScript==
(function() {
    ready(() => {
        waitForElement('#showTimerText')
            .then(element => {
                return new Promise(resolve => {
                    element.click();
                    resolve();
                });
            })
        ;
        
        // Because faucetcrypto short link have many version. Some of them have no button #showTimerText
        waitForElement('#timer').then(element => {
            waitForElementContentChange('#timer', '0').then(element => {
                document.getElementById('main-button').click();
            });
        });
    });
})();