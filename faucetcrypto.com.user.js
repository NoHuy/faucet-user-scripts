// ==UserScript==
// @name        faucetcrypto.com
// @namespace   Violentmonkey Scripts
// @match       https://faucetcrypto.com/*
// @require     http://localhost:3000/focusBypass.user.js
// @require     http://localhost:3000/lib/ready.js
// @require     http://localhost:3000/lib/waitForElement.js
// @grant       none
// @version     1.1
// @author      -
// @description 9/21/2020, 11:34:58 AM
// ==/UserScript==
(function() {
    ready(() => {
        let location = window.location.href;

        if (
            location.indexOf('task/faucet-claim') !== -1                  ||
            location.indexOf('achievement/complete/visit-coin-1') !== -1
        ) {
            waitForElementClassDisappear('main > div > div > div:nth-child(3) > div:nth-child(5) > span > button', 'cursor-not-allowed').then(element => {
                element.click();
            });
        } else if (location.indexOf('shortlink/list') !== -1) {
            let shortLinkCardElems = document.querySelectorAll('main div.grid.grid-responsive-3 > div');

            Array.from(shortLinkCardElems).forEach(elem => {
                let h3Elem = elem.querySelector('h3');
                let anchorElem = elem.querySelector('a');

                if (
                    (
                        h3Elem.innerText == 'Sh.faucetcrypto.com' ||
                        h3Elem.innerText == 'Sh.faucet.gold'      ||
                        h3Elem.innerText == 'Sh.claim4.fun'
                    ) &&
                    anchorElem
                ) {
                    anchorElem.click();

                    waitForElementClassDisappear('main > div > div > div:nth-child(3) > div:nth-child(5) > span > button', 'cursor-not-allowed').then(element => {
                        element.click();
                    });
                }
            });
        } else if (location.indexOf('ptc/list') !== -1) {
            waitForElement('main div.px-3.py-3 span a')
                .then(element => {
                    return new Promise(resolve => {
                        element.click();
                        setTimeout(resolve, 2000);
                    });
                })
                .then(() => {
                    return new Promise(resolve => {
                        waitForElementClassDisappear('main > div > div > div:nth-child(3) > div:nth-child(5) > span > button', 'cursor-not-allowed').then(element => {
                            element.click();
                        });

                        resolve();
                    });
                })
                .then(() => {
                    waitForElement('body > div.h-screen.flex > div > div > div.flex-1 > div.ml-2 > div > div.flex > span > a').then(element => {
                        element.click();
                        window.location.href = 'https://faucetcrypto.com/ptc/list';
                    });
                })
            ;
        }
    });
})();
