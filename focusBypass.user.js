// ==UserScript==
// @name         Focus bypass
// @namespace    Violentmonkey Scripts
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        *://*/*
// @grant        none
// ==/UserScript==
(function() {
    setInterval(function() {
        document.hasFocus = () => true;
        window.blurred = false;
    }, 300);
})();